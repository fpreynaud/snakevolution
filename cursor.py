import sys
from time import sleep

defaultDelay = .1
def write(s):
	ret = sys.stdout.write(s)
	sys.stdout.flush()
	return ret

def delayed(delay=defaultDelay):
	def wrapper2(func):
		def wrapper1(*args, **kw):
			sleep(delay)
			return func(*args, **kw)
		return wrapper1
	return wrapper2

def set(row,column):
	write('\x1b[{0};{1}H'.format(row, column))

def move_up(rows = 1):
	write('\x1b[{0}A'.format(rows))
	sys.stdout.flush()

def move_down(rows = 1):
	write('\x1b[{0}B'.format(rows))

def move_right(columns = 1):
	write('\x1b[{0}C'.format(columns))

def move_left(columns = 1):
	write('\x1b[{0}D'.format(columns))

class context:
	def __init__(self, row, column, width=10, height=10):
		self.bordersDrawn = False
		self.buffer = []
		self.startRow = row
		self.startColumn = column
		self.row = row
		self.column = column

		self.width = max(width, 10)
		self.height = max(height, 10)

		set(self.startRow+1, self.startColumn +1)
		self.row = self.startRow+1
		self.column = self.startColumn+1

	def __enter__(self):
		save()
		if not self.bordersDrawn:
			self.draw_borders()
		set(self.row, self.column)
		sys.stdout.flush()
		return self

	def __exit__(self,*args):
		#sleep(.1)
		restore()
		sys.stdout.flush()
		#sleep(.1)

	def clear(self):
		for row in range(self.startRow+1, self.startRow + self.height - 1):
			set_row(row)
			self.erase_line()

	def draw_borders(self):
		r = self.startRow
		set(r, self.startColumn)
		write('+' + '-'*(self.width-2) + '+')
		r += 1
		set(r, self.startColumn)
		for _ in range(self.height -2):
			write('|')
			move_right(self.width-2)
			write('|')
			r += 1
			set(r, self.startColumn)
		write('+' + '-'*(self.width-2) + '+')
		self.bordersDrawn = True

	def erase_line(self):
		set_column(self.startColumn + 1)
		write('\x1b[{0}X'.format(self.width-2))
		set_column(self.startColumn + 1)

	def print(self, *args, noNl = False):
		set(self.row, self.startColumn+1)
		if self.height >= 0 and self.row >= (self.startRow+1) + (self.height-2):
			self.scroll()

		self.buffer.append(' '.join(map(str, args)))
		self.erase_line()
		s = ' '.join(map(str, args))

		if self.width < 0:
			print(s)
		else:
			print(s[:self.width-2])

		if not noNl:
			self.row += 1
		sys.stdout.flush()

	def reset(self):
		self.row = self.startRow + 1
		self.column = self.startColumn + 1

	#@delayed()
	def scroll(self):
		self.reset()
		set(self.row, self.column)
		newBuffer = self.buffer[1:]
		self.buffer.clear()
		for line in newBuffer:
			self.erase_line()
			self.print(line)

def save():
	write('\x1b[s')

def restore():
	write('\x1b[u')

def set_column(column):
	write('\x1b[{0}G'.format(column))

def set_row(row):
	write('\x1b[{0}d'.format(row))

def erase_line():
	write('\x1b[2K')
	sys.stdout.flush()

def erase_lines(*rows):
	save()
	for row in rows:
		set_row(row)
		erase_line()
	restore()

if __name__ == '__main__':
	print('Bonjour')
	print()
	print()
	print()

	move_left(write('XYZ') + 1)
	move_up()
	write('U')
	move_left()
	move_right()
	write('R')
	move_left()
	move_up()
	write('U')
	move_left()
	move_left()
	write('L')
	move_left()
	print()
	print()
	print()

	set_row(30)
	write('YYYY')


