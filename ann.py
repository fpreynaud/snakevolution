from math import exp
import math
from settings import NNLayout
import random
from time import sleep

def sign(x):
	return 1 if x > 0 else 0 if x == 0 else -1

# Activation functions
def sigmoid(x):
	try:
		return 1/(1+exp(-x))
	except OverflowError as e:
		return 0

def ReLu(x):
	return max(0, x)

def tanh(x):
	return math.tanh(x)

class Neuron:
	def __init__(self, nbInputs):
		"""
		Randomly set weights and bias for the neuron
		"""
		self.weights = [random.uniform(-1,1) for _ in range(nbInputs)]
		self.bias = random.uniform(-1,1)

	def __eq__(self, n):
		return self.weights == n.weights and self.bias == n.bias
	
	def __repr__(self):
		s = 'Neuron{'
		s += f'bias:{round(self.bias,2)}, weights:{[round(w, 2) for w in self.weights]}}}'
		return s

	def fire(self, inputs, activationFunction=sigmoid):
		"""
		Calculate the output for this neuron
		"""
		assert len(inputs) == len(self.weights)
		return activationFunction(self.bias + sum(w*i for w,i in zip(self.weights, inputs)))

class NeuralNetwork:
	def __init__(self, nbInputs, layout = NNLayout):
		"""
		Create the neuron layers
		"""
		self.layers = []
		self.nbInputs = nbInputs

		# Add first hidden layer (connected to inputs)
		self.layers.append( [Neuron(nbInputs) for _ in range(layout[1])] )

		# Add other layers
		for i in range(2, len(layout)):
			self.layers.append( [Neuron(layout[i-1]) for _ in range(layout[i])] )

	def __eq__(self, nn):
		return self.layers == nn.layers and self.nbInputs == nn.nbInputs

	def feed(self, inputs):
		"""
		Calculate the output of the network from the provided inputs
		"""
		for layer in self.layers[:-1]:
			outputs = []
			for neuron in layer:
				neuronOutput = neuron.fire(inputs, activationFunction=ReLu)
				outputs.append(neuronOutput)
			inputs = outputs

		outputs = []
		for neuron in self.layers[-1]:
			neuronOutput = neuron.fire(inputs, activationFunction=sigmoid)
			outputs.append(neuronOutput)

		self.out = outputs

		return self.out

	@classmethod
	def from_chromosome(cls, nbInputs, chromosome, layout = NNLayout):
		"""
		Create a neural network based on a chromosome.
		A chromosome is a list of weight and biases, for each neuron
		e.g for a 2-2-2 network, with 3-dimensional inputs a chromosome would look like this (one line per neuron):
		[
			w(1,1)[0],w(1,1)[1],w(1,1)[2],b(1,1),
			w(1,2)[0],w(1,2)[1],w(1,2)[2],b(1,2),
			w(2,1)[0],w(2,1)[1],b(2,1),
			w(2,2)[0],w(2,2)[1],b(2,2),
			w(3,1)[0],w(3,1)[1],b(3,1),
			w(3,2)[0],w(3,2)[1],b(3,2)
		]
		"""

		# Make sure the chromosome has the adequate size to describe this particular network layout
		expected_chromosome_length = nbInputs*layout[1] + sum((layout[i] * layout[i-1] for i in range(2,len(layout)))) + sum(layout[1:])
		if len(chromosome) != expected_chromosome_length:
			print('NeuralNetwork.from_chromosome: Error: Delta between actual and expected chromosome lengths:', len(chromosome) - expected_chromosome_length)
			exit(1)

		# Initialize NeuralNetwork
		ann = cls(nbInputs, layout)

		# Copy weights and biases from the chromosome into the network
		for layer in ann.layers:
			for neuron in layer:
				neuron_inputs_count = len(neuron.weights)
				for i in range(neuron_inputs_count):
					neuron.weights[i] = chromosome[i]
				neuron.bias = chromosome[neuron_inputs_count]
				chromosome = chromosome[neuron_inputs_count+1:]

		return ann
