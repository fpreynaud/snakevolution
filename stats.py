stats = {}
def add(name):
	if name not in stats:
		stats[name] = []

def get(name):
	return stats.get(name, [])

def update(name, value):
	stats[name].append(value)
