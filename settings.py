## Neural network settings
# Snake vision
visionInputLength = 3
visionPrecision = 2
nInputs = 2*visionPrecision*visionInputLength + 2

# Neural network layout
NNLayout = [2,2,4]

## Evolution settings
# Probability that a gene will mutate
mutationRate = 0.25

# Proportion of current population to keep in next generation
replacement_rate = 0.5

# Size of chromosome. Used for stats
genomeSize = sum(NNLayout) + nInputs * NNLayout[0] + sum(NNLayout[i]*NNLayout[i+1] for i in range(len(NNLayout) - 1))

# Number of individuals in the population
populationSize = 500

# TTL for the population
timeToExtinction = 1000

# Tournament size
tournament_size = max(2,int(populationSize * 0.15))

## Display settings
# Tells if each play should be displayed. Funny to see, but slows things down
viewable = False
# Time to wait between each frame
delay = 0.300

gridSize = 10

## Display regions
individualRegion = (1, gridSize + 20)
globalStatsStart = (1, gridSize + 115)
generationStatsRegionStart = (max(12,gridSize) + 6, 1)

