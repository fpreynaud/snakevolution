from math import sin,cos,pi,asin,acos,sqrt
from time import sleep,time
import os
import random

from matplotlib import pyplot as plt

from ann import NeuralNetwork
from GA import Population,Individual,happen
from settings import NNLayout, mutationRate, gridSize, individualRegion, populationSize, nInputs, delay, viewable, visionPrecision
import cursor
import stats

gridDisplayRegionStart = (1,1)

def sign(x):
	return 1 if x > 0 else -1 if x < 0 else 0

class Grid(list):
	def __init__(self, size=5):
		"""
		Create square grid of given size
		"""
		list.__init__(self)
		self.size = size
		self.appleSeed = random.randint(-1000000000, 1000000000)
		self.appleGenerator = random.Random(self.appleSeed)

		for _ in range(size**2):
			self.append(' ')

	def clear(self):
		"""
		Clear grid contents
		"""
		for i,_ in enumerate(self):
			self[i] = ' '

	def display(self):
		"""
		Draw grid on screen
		"""
		with cursor.context(*gridDisplayRegionStart, width=2 + 2*self.size + 2 + 2, height=self.size+3) as cc:
			l = [j%10 for j in range(1, self.size+1)]
			#cc.print('   {0: >2}'.format(' '.join(map(str,l))))
			for i in range(0, self.size**2, self.size):
				cc.print('{1: >2}|{0}|'.format(''.join(self[i:i+self.size]),int(i/self.size)+1))

	def get(self, row, column):
		"""
		Get contents at given coordinates
		"""
		return self[row * self.size + column]

	def place_apple(self):
		"""
		Place an apple at a random free location on the grid
		"""
		freeSpaces = []
		for i,space in enumerate(self):
			if space == ' ':
				freeSpaces.append(i)
		if freeSpaces == []:
			cursor.set_row(25)
			print('You won !')
		else:
			applePosition = self.appleGenerator.choice(freeSpaces)
			#Snake.ir.print(applePosition)
			self[applePosition] = 'A'

	def set(self, row, column, value):
		"""
		Set contents at given coordinates
		"""
		self[row * self.size + column] = value

class Snake(list, Individual):
	ir = cursor.context(*individualRegion,width=85)

	# Construction
	def __init__(self, gridSize=5):
		"""
		Create a snake with brain as defined in the settings
		"""
		list.__init__(self)
		self.brain = NeuralNetwork(nbInputs = nInputs, layout = NNLayout)
		Individual.__init__(self, self.brain)
		self.reset_state()

		self.grid = Grid(gridSize)
		self.history = []

		self.reset_position()
		self.startingPosition = self[0]
		self.grid.place_apple()
		self.turns = 0

	@classmethod
	def from_individual(cls, individual, gridSize=5):
		"""
		Create a Snake from an Individual
		"""
		snake = cls(gridSize)
		snake.brain = NeuralNetwork.from_chromosome(nbInputs = nInputs, layout = NNLayout, chromosome = individual.chromosome)
		snake.chromosome = individual.chromosome
		return snake

	# reset methods
	def reset(self):
		"""
		Reset the snake so it can play again
		Some individuals are kept between generation, but the only thing we actually want to keep as-is is the brain.
		"""
		self.reset_state()
		self.grid.clear()
		self.clear()
		self.reset_position()
		self.grid.place_apple()
		self.turns = 0
		self._fitness = 0
		self.brain = NeuralNetwork.from_chromosome(
			chromosome = self.chromosome, 
			nbInputs = nInputs, 
			layout = NNLayout
		)

	def reset_position(self):
		"""
		Reset snake's position on the grid
		"""
		row,column = random.randint(0, self.grid.size-1), random.randint(1, self.grid.size-1)
		self.append((row,column))
		self.append((row,column-1))
		self.draw()

	def reset_state(self):
		"""
		Reset some of the snake's state variables
		"""
		self.age = 0
		self.alive = True
		self.direction = (0,1)
		self.grow = False
		self.stamina = 50
		self.appleScore = 0
		self.score = 0
		self.turns = 0

	# Drawing
	def draw(self):
		"""
		Draw snake on screen
		"""
		row,column = self[0]
		replaceApple = False
		self.grow = False
		headSprite = {
			(0,-1):'\u25C0',
			(0,1):'\u25B6',
			(-1,0):'\u25B2',
			(1,0):'\u25BC'
		}

		if self.grid.get(row, column) == 'A':
			replaceApple = True

		#Draw head
		self.grid.set(row, column, headSprite[self.direction])

		# Draw intermediary parts
		for i,(row,column) in enumerate(self[1:-1]):
			part = self.get_part_sprite(previousPart=self[i],nextPart = self[i+2],currentPart = (row,column))

			self.grid.set(row ,column, part)

		#Draw tail
		self.grid.set(self[-1][0] ,self[-1][1], '\u25AF')

		if replaceApple:
			self.grow = True
			self.appleScore += 1
			self.stamina += 10
			self.grid.place_apple()

	def get_part_sprite(self, previousPart, nextPart, currentPart):
		"""
		Helper function for drawing the snake
		"""
		previousRow, previousColumn = previousPart
		nextRow, nextColumn = nextPart
		row, column = currentPart
		previousDir = (previousRow - row, previousColumn - column)
		nextDir = (nextRow - row, nextColumn - column)
		up,down,left,right = (-1,0), (1,0), (0,-1), (0,1)

		if (previousDir,nextDir) in ((up,down),(down,up)):
			sprite = '\u2551'
		elif (previousDir,nextDir) in ((up,left),(left,up)):
			sprite = '\u255d'
		elif (previousDir,nextDir) in ((up,right),(right,up)):
			sprite = '\u255a'
		elif (previousDir,nextDir) in ((down,left),(left,down)):
			sprite = '\u2557'
		elif (previousDir,nextDir) in ((down,right),(right,down)):
			sprite = '\u2554'
		else:
			sprite = '\u2550'

		return sprite

	# Genetic-algorithm-related
	def __add__(self, snake):
		"""
		Add own fitness to another snake's fitness
		"""
		return Individual.__add__(self, snake)

	def __radd__(self, snake):
		"""
		Add another snake's fitness to own fitness
		"""
		return Individual.__radd__(self, snake)

	def fitness(self):
		"""
		Calculate snake's fitness
		"""
		# Fitness criteria:
		#
		# - Apples eaten
		# - Long life
		f = self.appleScore + self.age
		return f

	def mate(self, partner):
		"""
		Mate with another snake
		"""
		child1,child2 = Individual.mate(self,partner)
		child1 = Snake.from_individual(child1, gridSize = gridSize)
		child2 = Snake.from_individual(child2, gridSize = gridSize)

		return child1,child2

	def mutate(self):
		"""
		Randomly mutate some chromosomes
		"""
		chromosome = []
		for layer,_ in enumerate(self.brain.layers):
			for neuron,_ in enumerate(self.brain.layers[layer]):
				for i,_ in enumerate(self.brain.layers[layer][neuron].weights):
					if happen(mutationRate):
						self.brain.layers[layer][neuron].weights[i] += random.uniform(-1,1)
				chromosome += self.brain.layers[layer][neuron].weights

				if happen(mutationRate):
					self.brain.layers[layer][neuron].bias = random.uniform(-1,1)
				chromosome.append(self.brain.layers[layer][neuron].bias)
		self.chromosome = chromosome

	# Playing
	def live(self, delayed=False):
		"""
		Play the game and update fitness
		"""
		directions = ((-1,0), (1,0), (0,-1), (0,1)) # Up, down, left, right
		self.randomState = random.getstate()
		#Snake.ir.clear()
		#Snake.ir.reset()
		self.draw()
		if delayed:
			self.grid.display()

		while self.alive:
			if delayed:
				sleep(delay)

			# Get inputs for the neural network
			inputs = self.look_around()
			inputs += self.direction

			# Feed inputs to the neural network to decide next move
			up,down,left,right = self.brain.feed(inputs)
			i,_ = max(enumerate((up,down,left,right)), key=lambda t:t[1])
			#if delayed:
				#with Snake.ir as cc:
					#dirs = 'UP,DOWN,LEFT,RIGHT'.split(',')
					#if directions[i] != self.direction and directions[i] != (-self.direction[0], -self.direction[1]):
						#cc.print('Go', dirs[i])
			if self.direction != directions[i]:
				self.turns += 1
			self.direction = directions[i]

			# Perform move
			self.move(self.direction)

			self.draw()
			if delayed:
				self.grid.display()

		# Calculate fitness
		self._fitness = self.fitness()
		if viewable:
			with Snake.ir as cc:
				cc.print('Fitness:',self._fitness)
		self.history.append(self._fitness)

	def move(self, direction):
		"""
		Move in the given direction
		"""
		# Die if move is invalid
		if not self.is_valid_move(direction):
			if viewable:
				with Snake.ir as cc:
					cc.print('GAME OVER')
					cc.print('Score:', self.score)
					cc.print('Age:',self.age)
			return

		row,col = self[0]
		drow,dcol = direction
		tail = self[-1]

		for i in range(len(self)-1,0,-1):
			self[i] = self[i-1]

		self[0] = (row + drow, col + dcol)

		if self.grow:
			self.append(tail)
		else:
			self.grid.set(tail[0], tail[1], ' ')
			self.stamina -= 1

		self.age += 1

	def is_valid_move(self, direction):
		"""
		Check if snake can move in given direction
		"""
		row,col = self[0]
		drow,dcol = direction

		if self.stamina <= 0:
			self.age -= 50

		if (row + drow, col + dcol) in self\
		or row + drow < 0\
		or row + drow >= self.grid.size\
		or col + dcol < 0\
		or col + dcol >= self.grid.size\
		or self.stamina <= 0:
			self.alive = False

		return self.alive

	def look_around(self):
		"""
		Look at surroundings at various angles
		"""
		visionInputs = []

		angle = self.get_angle_from_direction(self.direction)
		precision = max(2,visionPrecision)

		for i in range(2*precision):
			angle += i * pi/precision
			visionInputs += self.look_in_direction(angle)

		return visionInputs

	def look_in_direction(self, angle):
		"""
		Look at a given angle relative to current direction
		Return what's seen as inputs to the neural network
		"""
		# Derive the vision line slope from the angle
		# In other words convert line equation in polar coordinates to equation in cartesian coordinates
		dx,dy = cos(angle), sin(angle)
		x0,y0 = self[0][1], self[0][0]
		x = x0
		y = y0
		error = 0
		distanceToWall = 0
		distanceToSelf = self.grid.size**2
		minDistanceToSelf = False
		applePresent = False

		# Draw line at given angle
		while x >= 0 and x < self.grid.size and y >= 0 and y < self.grid.size:
			if dx == 0:
				y += sign(dy)
			else:
				m = dy/dx
				if abs(m) <= 1:
					x += sign(dx)
					error += m
					if abs(error) > 0.5:
						error -= sign(m)
						y += sign(dy)
				else:
					y += sign(dy)
					m = 1/m
					error += m
					if abs(error) > 0.5:
						error -= sign(m)
						x += sign(dx)
				if x <0 or x >= self.grid.size or y <0 or y >= self.grid.size:
					break

			row,column = y,x
			if self.grid.get(row, column) == 'A':
				applePresent = True
			#self.grid.set(row, column, 'x') 
			#self.grid.display()
			#sleep(.2)
			if (row, column) in self:
				distanceToSelf = min(distanceToSelf, abs(row - y0) + abs(column - x0))
			distanceToWall = 1 + abs(row - y0) + abs(column - x0)

		return [distanceToWall, distanceToSelf, applePresent]

	def get_angle_from_direction(self, direction):
		"""
		Convert cartesian coordinates to polar
		"""
		dy,dx = direction
		r = sqrt(dx**2 + dy**2)

		return asin(dy/r) - dx*acos(dx/r)

	def get_direction_from_angle(self,angle):
		"""
		Convert polar coordinates to cartesian
		"""
		dy, dx = sin(angle), cos(angle)

		if abs(dy) < 1e-10:
			dy = 0
		if abs(dx) < 1e-10:
			dx = 0
		if dy != 0:
			dy = int(dy/abs(dy))
		if dx != 0:
			dx = int(dx/abs(dx))

		return (dy,dx)

	# Save and load
	def save(self):
		"""
		Save snake to file
		"""
		import json
		state = self.randomState
		d = {
			'fitness':self._fitness,
			'startingPosition':self.startingPosition,
			'appleSeed':self.grid.appleSeed,
			'brain':self.chromosome,
			'randomState':state
		}
		with open('best_snake.json','w') as f:
			json.dump(d, f)

	@classmethod
	def load(cls, filename='best_snake.json'):
		"""
		Load snake from file
		"""
		import json
		d = json.load(open(filename))
		#state = d['randomState']
		#state[1] = tuple(state[1])
		#state = tuple(state)
		brain = d['brain']
		#random.setstate(state)
		snake = cls(gridSize=gridSize)
		snake.brain = NeuralNetwork.from_chromosome(nbInputs = nInputs, chromosome = brain)
		r, c = d['startingPosition']
		snake.grid.clear()
		snake.clear()
		snake.append((r,c))
		snake.append((r,c-1))
		snake.startingPosition = (r,c)
		snake.draw()

		snake.grid.appleSeed = d['appleSeed']
		snake.grid.appleGenerator = random.Random(snake.grid.appleSeed)
		snake.grid.place_apple()
		return snake

if __name__ == '__main__':
	print('\x1b[H\x1b[2J')

	# Initialize snake population
	snakes = list(Snake(gridSize) for _ in range(populationSize))
	population = Population(snakes)

	try:
		# Evolve snake population
		population.evolve()
	finally:
		# Show final stats and replay best snake
		#plt.plot( stats.get('generation'),stats.get('averageFitness'),'')
		#plt.show()
		bestSnake = Snake.load()
		bestSnake.live(delayed=True)
