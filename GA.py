from settings import *
from log import debug
import random
import cursor
import functools
import stats

gsc = cursor.context(*generationStatsRegionStart, width=100)

def happen(rate):
	return random.random() <= rate

def median(values, sort = True, key=None):
	n = len(values)
	l = sorted(values, key=key) if sort else values

	return l[n//2] if n % 2 == 1 else (l[n//2 - 1] + l[n//2])/2

class Individual:
	def __add__(self, individual):
		if type(individual) in (int,float):
			return self._fitness + individual
		return self._fitness + individual._fitness

	def __ge__(self, individual):
		return self._fitness >= individual._fitness

	def __gt__(self, individual):
		return self._fitness > individual._fitness

	def __init__(self, ann=None):
		debug(__file__, 'Creating individual')
		self.chromosome = []
		self.TTL = 100
		self._fitness = 0

		# Create chromosome from neural network
		if ann is not None:
			for layer in ann.layers:
				for neuron in layer:
					self.chromosome.extend(neuron.weights)
					self.chromosome.append(neuron.bias)

	def __lt__(self, individual):
		return self._fitness < individual._fitness

	def __le__(self, individual):
		return self._fitness <= individual._fitness

	def __radd__(self, individual):
		if type(individual) in (int,float):
			return self._fitness + individual
		return individual.__add__(self)

	def live(self):
		raise NotImplementedError

	def mate(self, individual):
		debug('Mating', self, 'with', individual)
		offspring = self.single_point_crossover(individual)
		return offspring

	def mutate(self, mutationPropability=mutationRate):
		for i in range(len(self.chromosome)):
			if happen(mutationPropability):
				debug('Mutating chromosome', i, 'on', self)
				self.chromosome[i] += random.uniform(-0.25,0.25)

	def biased_uniform_crossover(self, individual):
		# Uniform crossover biased toward the fittest parent
		debug('Using biased uniform crossover')
		offspring = [Individual(),Individual()]
		for child in offspring:
			for i in range(len(self.chromosome)):
				if happen(self._fitness/(self._fitness + individual._fitness)):
					child.chromosome.append(self.chromosome[i])
				else:
					child.chromosome.append(individual.chromosome[i])

		return offspring


	def single_point_crossover(self, individual):
		child1,child2 = Individual(),Individual()

		crossoverPoint = random.randrange(len(self.chromosome))
		child1.chromosome = self.chromosome[:crossoverPoint]\
		            + individual.chromosome[crossoverPoint:]

		child2.chromosome = \
			individual.chromosome[:crossoverPoint]\
		      + self.chromosome[crossoverPoint:]

		return child1,child2

	def uniform_crossover(self, individual):
		child1,child2 = Individual(),Individual()
		for i in range(len(self.chromosome)):
			if happen(0.5):
				child1.chromosome.append(self.chromosome[i])
				child2.chromosome.append(individual.chromosome[i])
			else:
				child1.chromosome.append(individual.chromosome[i])
				child2.chromosome.append(self.chromosome[i])

		return child1,child2

class Population(list):

	def __init__(self, individuals):
		debug(__file__, 'Creating population')
		list.__init__(self, individuals)
		self.size = len(individuals)
		self.bestFitness = 0
		self.averageFitness = 0
		self.highScore = 0

	def build_next_generation(self):
		"""
		Create the next generation of individual via selection, crossover and mutation
		"""
		debug('Building next generation')
		nextGen = []

		# Select parents for mating
		parents = self.select(min(0.5,replacement_rate)*len(self))

		# Keep selected parents in the next generation
		nextGen.extend(parents)

		# Add offspring created from parents in the current generation to the next generation
		for i in range(len(parents) - 1):
			if len(nextGen) >= self.size:
				break
			children = parents[i].mate(parents[i+1])
			for child in children:
				child.mutate()
			if self.size - len(nextGen) >= len(children):
				nextGen.extend(children)
			else:
				nextGen.append(random.sample(children, self.size-len(children)))
		if len(nextGen) < self.size:
			nextGen.extend(random.sample(self, self.size - len(nextGen)))

		# Replace the current generation with the next
		self.clear()
		self.extend(nextGen)

	def distance(self, chromosome1, chromosome2):
		return functools.reduce(lambda x,y:x+abs(y[0]-y[1]), zip(chromosome1,chromosome2), 0)/len(chromosome1)

	def evaluate(self):
		"""
		Evaluate each individual in the population
		"""
		debug('Evaluating the population')
		with gsc:
			gsc.reset()
			gsc.print('Generation', self.generation)

			bestChildFitness = 0
			for i,individual in enumerate(self):
				individual.TTL -= 1
				gsc.print('Individual', i+1, '/' , len(self),noNl = True)

				fitness = 0
				individual.live(delayed=viewable)

				# Save individual for replay
				if individual._fitness > self.bestFitness:
					individual.save()

				bestChildFitness = max(bestChildFitness, individual._fitness)

			stats.update('bestChild',bestChildFitness)
			self.averageFitness = sum(self)/len(self)
			gsc.row += 1
			gsc.print('Generation best:', max(individual._fitness for individual in self))
			gsc.print('Best Child:', bestChildFitness)
			gsc.print('Average fitness:', self.averageFitness)
			gsc.print('Median fitness:', median([individual._fitness for individual in self], sort=True))
			#diversity = self.get_diversity()
			#gsc.print('Diversity:', diversity)
			#stats.update('diversity', diversity)

	def evolve(self):
		"""
		Evolve a population of individuals
		"""
		debug(__file__, 'Starting evolution')
		self.TTE = timeToExtinction
		self.generation = 1
		stats.add('generation')
		stats.add('generationBest')
		stats.add('bestChild')
		stats.add('averageFitness')
		stats.add('diversity')
		global viewable

		while self.TTE > 0:
			try:
				generationBest = 0
				self.print_global_stats()

				# Evaluate all the individuals in the population
				self.evaluate()

				## Update stats
				generationBest = max(individual._fitness for individual in self)
				diversity = self.get_diversity()
				stats.update('generation',self.generation)
				stats.update('generationBest',generationBest)
				stats.update('averageFitness',self.averageFitness)

				## Reset timeout if the population is improving
				if generationBest > self.bestFitness:
					self.bestFitness = generationBest
					self.TTE = max(self.generation,timeToExtinction)

				# Create the next generation
				self.build_next_generation()

				self.generation += 1
				self.TTE -= 1
			except KeyboardInterrupt:
				answer = input('Continue ?')
				if answer.lower() == 'y':
					viewable = not viewable
				else:
					break

	def get_central_chromosome(self):
		genomeSize = len(self[0].chromosome)
		centralChromosome = [0]*genomeSize

		for individual in self:
			for i,gene in enumerate(individual.chromosome):
				centralChromosome[i] += gene

		centralChromosome = list(map(lambda x:x/len(self), centralChromosome))

		return centralChromosome

	def get_diversity(self):
		cc = self.get_central_chromosome()
		diversity = 0

		for individual in self:
			diversity += self.distance(cc, individual.chromosome)

		return diversity/len(self)

	def print_global_stats(self):
		with cursor.context(*globalStatsStart, width=40) as cc:
			cc.print('Mutation rate:', mutationRate)
			cc.print('Genome size', genomeSize)
			cc.print('Brain layout', [nInputs] + NNLayout)
			cc.print('Best fitness', self.bestFitness)
			cc.print('ETA:', self.TTE)

	def select(self, count=1):
		debug('Selecting', count, 'individuals for reproduction')
		selected = []
		while len(selected) < count:
			selected.append(self.tournament(size=tournament_size))
		return selected

	def roulette_wheel(self):
		"""
		Select one individual via roulette wheel
		"""
		total_fitness = sum(individual._fitness for individual in self)
		spin = random.uniform(0, total_fitness)
		cumulative_fitness = 0

		for i,individual in enumerate(self):
			cumulative_fitness += individual._fitness
			if cumulative_fitness >= spin:
				debug('Selected individual', i, 'via roulette wheel')
				return individual

	def tournament(self, size=2):
		"""
		Select one individual via tournament
		"""
		participants = list(self)
		participants = random.sample(self, size)
		return max(participants, key=lambda individual: individual._fitness)
